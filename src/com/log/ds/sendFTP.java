package com.log.ds;


import java.io.IOException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;


import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPConnectionClosedException;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;
import org.apache.log4j.Logger;

public class sendFTP {
	private static final Logger LOG = Logger.getLogger(sendFTP.class);
	/*public static String ftpUrl = "ftp://%s:%s@%s/%s;type=i";
	public static String host = "pruftp.aseguramientosalud.com:21";
	public static String user = "Usutransar";
	public static String pass = "C0ns0rc102015*";
	public static String filePath = "C:/Users/Administrator.COMPENSAR/Desktop/datos.zip";
	public static String uploadPath = "datos.zip";
	public static final int BUFFER_SIZE = 4096;
	public static boolean archivosEnviado = false;
	public static String dirFtp = "transenlinea";*/

	
	
	 public static  boolean InvokeSendFTP(String hostPARAM,String userPARAM,
				String passPARAM,String filePathPARAM,String uploadPathPARAM,
				String directoriFtp,String protocolo,int puerto) 
	    {
	        int base = 0;
	        boolean storeFile = true, binaryTransfer = true, error = true;
	        String server, username, password, remote, local;
	        String protocol = protocolo;//"TLS";    // SSL/TLS
	        FTPSClient ftps;
	        
	        server = hostPARAM;//"190.144.216.44";
	        LOG.info("eror InvokeSendFTP Value hostPARAM   "+hostPARAM);
    		LOG.debug("eror InvokeSendFTP Value hostPARAM   "+hostPARAM);
	        username = userPARAM;//"consorciodc\\usuftpapp";
	        LOG.info("eror InvokeSendFTP Value userPARAM   "+userPARAM);
    		LOG.debug("eror InvokeSendFTP Value userPARAM   "+userPARAM);
	        password = passPARAM;//"4ppS4ludpru";
	        LOG.info("eror InvokeSendFTP Value passPARAM   "+passPARAM);
    		LOG.debug("eror InvokeSendFTP Value passPARAM   "+passPARAM);
	       
	        remote = directoriFtp+"/"+uploadPathPARAM;//"/appsalud/test.zip";
	        LOG.info("eror InvokeSendFTP Value remote   "+remote);
    		LOG.debug("eror InvokeSendFTP Value remote   "+remote);
    		
	        local = filePathPARAM;//"C:/KonyMobileFabricServer/CompensarPDFs/test.zip";// "C:\\Users\\soluciones\\Desktop\\Isaac\\Workspaces_Kony\\Workspace_603U\\COMPENSAR_WS\\COMPENSAR2.59_PASEAPRUEBAS\\maquillaje.jpg";
	        LOG.info("eror InvokeSendFTP Value filePathPARAM   "+filePathPARAM);
    		LOG.debug("eror InvokeSendFTP Value filePathPARAM   "+filePathPARAM);
	        ftps = new FTPSClient("TLS",true);//Protocolo TLS, isImplicit == true
	        //ftps.setDefaultPort(990);
	        LOG.info("eror InvokeSendFTP Value puerto   "+puerto);
    		LOG.debug("eror InvokeSendFTP Value puerto   "+puerto);
	       // ftps.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out),true));
	        System.out.println("Connecting to " + server + ".");
	        try
	        {
	            int reply;

	            ftps.connect(server,puerto);// IP y puerto
	            System.out.println("Connected to " + server + ".");

	            // After connection attempt, you should check the reply code to verify
	            // success.
	            reply = ftps.getReplyCode();
	            ftps.setConnectTimeout(999999999);

	            if (!FTPReply.isPositiveCompletion(reply))
	            {
	                ftps.disconnect();
	                System.err.println("FTP server refused connection.");
	                System.exit(1);
	            }
	        }
	        catch (IOException e)
	        {
	            if (ftps.isConnected())
	            {
	                try
	                {
	                    ftps.disconnect();
	                }
	                catch (IOException f)
	                {
	                    // do nothing
	                }
	            }
	            LOG.info("eror InvokeSendFTP Value ftpUrl   ex.toString("+e.toString());
	    		LOG.debug("eror InvokeSendFTP Value ftpUrl   ex.toString("+e.toString());
	            System.err.println("Could not connect to server.");
	            e.printStackTrace();
	            System.exit(1);
	        }

	//__main:
	        try
	        {
	            ftps.setBufferSize(4096);//1000000000
	            //ftps.allo(100000);
	            if (!ftps.login(username, password))
	            {
	                ftps.logout();
	                error = false;
	               // break __main;
	                LOG.info("eror InvokeSendFTP Value ftpUrl   error login");
		    		LOG.debug("eror InvokeSendFTP Value ftpUrl   error login");
	            }
	            ftps.enterLocalPassiveMode();

	            if (storeFile)
	            {
	            	
	            	//execPBSZ
	            	ftps.execPBSZ(0);
	            	ftps.execPROT("P");
	            	ftps.setFileType(FTP.BINARY_FILE_TYPE);
	            	File localFile = new File(local);
	            	InputStream inputStream = new FileInputStream(localFile);	            	
	            	ftps.storeFile(remote, inputStream);            	
	            	inputStream.close();	            	
	            
	            }
	            else
	            {
	                
	            	OutputStream output;

	                output = new FileOutputStream(local);

	                ftps.retrieveFile(remote, output);

	                output.close();
	            }

	            ftps.logout();
	        }
	        catch (FTPConnectionClosedException e)
	        {
	            error = false;
	            System.err.println("Server closed connection.");
	            e.printStackTrace();
	            LOG.info("eror InvokeSendFTP Value ftpUrl   ex.toString("+e.toString());
	    		LOG.debug("eror InvokeSendFTP Value ftpUrl   ex.toString("+e.toString());
	        }
	        catch (IOException e)
	        {
	            error = false;
	            e.printStackTrace();
	            LOG.info("eror InvokeSendFTP Value ftpUrl   ex.toString("+e.toString());
	    		LOG.debug("eror InvokeSendFTP Value ftpUrl   ex.toString("+e.toString());
	    
	        }
	        finally
	        {
	            if (ftps.isConnected())
	            {
	                try
	                {
	                    ftps.disconnect();
	                }
	                catch (IOException f)
	                {
	                    // do nothing
	                }
	            }
	        }

	        //System.exit(error ? 1 : 0);
	        LOG.info(" InvokeSendFTP Value final   "+error);
    		LOG.debug(" InvokeSendFTP Value final   "+error);
    
			return error;
	    } // end main
	
	/*
	public static boolean InvokeSendFTP(String hostPARAM,String userPARAM,
			String passPARAM,String filePathPARAM,String uploadPathPARAM,String directoriFtp){		

		host = hostPARAM;//"pruftp.aseguramientosalud.com:21";
		user = userPARAM;//"Usutransar";
		pass = passPARAM;//"C0ns0rc102015*";
		filePath = filePathPARAM;//"C:/Users/Administrator.COMPENSAR/Desktop/datos.zip";
		uploadPath = uploadPathPARAM;//"datos.zip";
		dirFtp = directoriFtp;
		LOG.info("InvokeSendFTP Value host is: " +host);
		LOG.debug("InvokeSendFTP Value host is: " +host);
		
		LOG.info("InvokeSendFTP Value user is: " +user);
		LOG.debug("InvokeSendFTP Value user is: " +user);
		
        //ftpUrl = String.format(ftpUrl, user, pass, host, uploadPath);
		//ftpUrl = "ftp://"+user+":"+pass+"@"+host+"/transenlinea/"+uploadPath+";type=i";
		ftpUrl = "ftp://"+user+":"+pass+"@"+host+dirFtp+"/"+uploadPath+";type=i";
        System.out.println("Upload URL: " + ftpUrl);
        LOG.info("InvokeSendFTP Value ftpUrl is: " +ftpUrl);
		LOG.debug("InvokeSendFTP Value ftpUrl is: " +ftpUrl);
 
        try {
            URL url = new URL(ftpUrl);
            URLConnection conn = url.openConnection();
            OutputStream outputStream = conn.getOutputStream();
            FileInputStream inputStream = new FileInputStream(filePath);
 
            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = -1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
 
            inputStream.close();
            outputStream.close();
            archivosEnviado = true;
            LOG.info("InvokeSendFTP Value ftpUrl archivosEnviadois: " +archivosEnviado);
    		LOG.debug("InvokeSendFTP Value ftpUrl archivosEnviadois: " +archivosEnviado);
            System.out.println("File uploaded");
        } catch (IOException ex) {
            ex.printStackTrace();
            System.out.println("eror FTP File uploaded"+ex.toString());
            archivosEnviado = false;
            LOG.info("eror InvokeSendFTP Value ftpUrl archivosEnviadois: " +archivosEnviado+" ex.toString("+ex.toString());
    		LOG.debug("eror InvokeSendFTP Value ftpUrl archivosEnviadois: " +archivosEnviado+" ex.toString("+ex.toString());
            
        }finally{
        	return 	archivosEnviado;
        }
		
	}*/

	
}
