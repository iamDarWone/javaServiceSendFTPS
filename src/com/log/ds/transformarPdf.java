package com.log.ds;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;

import Decoder.BASE64Decoder;

public class transformarPdf {
	private static final Logger LOG = Logger.getLogger(transformarPdf.class);
	
	
	public static boolean convertir(String encodedBase64,String ruta,String nombrePdf) {
		LOG.info("convertir transformarPdf encodedBase64 is: " +encodedBase64);
		LOG.debug("convertir transformarPdf encodedBase64 is: " +encodedBase64);
		
		 LOG.info("convertir ruta Value is: " +ruta);
		 LOG.debug("convertir ruta Value is: " +ruta);
		 
		 LOG.info("convertir nombrePdf Value is: " +nombrePdf);
		 LOG.debug("convertir nombrePdf Value is: " +nombrePdf);
		 try {
			 if("".equals(encodedBase64) ||" ".equals(encodedBase64) || encodedBase64==null){
				  return false; 
			  }else{
				  	BASE64Decoder decoder = new BASE64Decoder();
					 byte[] decodedBytes;
				
					decodedBytes = decoder.decodeBuffer(encodedBase64);
				

					File file = new File(ruta+nombrePdf+".pdf");
					FileOutputStream fop = new FileOutputStream(file);

		         	fop.write(decodedBytes);
		         	fop.flush();
		         	fop.close();
		         	 LOG.info("convertir transformarPdf  ok: " +file.getPath());
		    		 LOG.debug("convertir transformarPdf ok: " +file.getPath());
		         	return true;
			  }
		 } catch (IOException e) {				
			 	LOG.info("convertir IOException Value is: " +e);
			 	LOG.debug("convertir IOException Value is: " +e);
				return false;
			}
		
	}
	
}
