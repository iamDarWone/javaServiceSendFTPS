package com.log.ds;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Date;

import org.apache.log4j.Logger;

public class LogKony {
	private static final Logger LOG = Logger.getLogger(LogKony.class);
	static  String _PATH = "";// configurar la ruta en servidor
	static  String _IDDOCUMENT = "";// nombre del documento que se genera
	
	public static boolean beginFile(String path,String name, String data)  {
		
		_PATH = path;
		_IDDOCUMENT = name;
		String repo = data;
		
		try {
				/**for (int i = 0; i < data.length; i++) {
					repo += "|"+data[i].toString();
					//System.out.println(data[i].toString());
				}*/
				Date fechaActual = new Date();
				//1 validate path 
				if((!path.isEmpty())	&&  (!name.isEmpty() )){
					 if(existFile()){//file finded 
						 	
					       // System.out.println(fechaActual);
	
						 System.out.println(" archivo encontrado escribiendo  "); 
						 //to write 1 line with the data  content
						 writeFile(repo) ;
					 }else{
						 System.out.println(" archivo  generado por primera vez "); 
						 // to generate the new file 
						 generateFile();
						 writeFile(repo) ;
					 }			
					 LOG.info("Filerror  Value is true " );
					 LOG.debug("Filerror   Value is:  true" );
					 return true;
					
				}
							
		} catch (Exception e) {
			LOG.info("Filerror Exception false Value is: " +e.toString());
			LOG.debug("Filerror  Exception false Value is: " +e.toString());
			return false;
		}
		LOG.info("Filerror  false Value is: " );
		LOG.debug("Filerror   false Value is: " );
		
		return false;
		
	}
	
	public static void generateFile()  {
		 File Ffichero = new File(_PATH+"/"+_IDDOCUMENT);
		 try {
			Ffichero.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println(" error generateFile "+e.toString()); 
		}
	       
	          
	        
	}
	
	public static boolean existFile()  {
		
		File archivo = new File(_PATH+"/"+_IDDOCUMENT);		
		if(archivo.exists()) {
			 System.out.println(" file exist "); 
			return true;
		} else {
			 System.out.println(" file  no exist "); 
			return false;
		}
		
		
	}
	/**
	 * this fuction write one line like this
	 * date-code transaction-type-title-description
	 * 
	 * @return
	 
	*/
	public static void writeFile(String SCadena){
		  try {
			  File Ffichero = new File(_PATH+"/"+_IDDOCUMENT);	
		          //Si no Existe el fichero lo crea
		           if(!Ffichero.exists()){
		               Ffichero.createNewFile();
		           }
		          /*Abre un Flujo de escritura,sobre el fichero con codificacion utf-8. 
		           *Además  en el pedazo de sentencia "FileOutputStream(Ffichero,true)",
		           *true es por si existe el fichero seguir añadiendo texto y no borrar lo que tenia*/
		          BufferedWriter Fescribe=new BufferedWriter(new OutputStreamWriter(new FileOutputStream(Ffichero,true), "utf-8"));
		          /*Escribe en el fichero la cadena que recibe la función. 
		           *el string "\r\n" significa salto de linea*/
		          Fescribe.write(SCadena + "\r\n");
		          //Cierra el flujo de escritura
		          Fescribe.close();
		       } catch (Exception ex) {
		          //Captura un posible error le imprime en pantalla 
		          System.out.println(ex.getMessage());
		         // return false;
		       } 
			//return true;
		}
}
