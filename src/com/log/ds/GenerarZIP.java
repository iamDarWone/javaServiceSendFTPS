package com.log.ds;

import java.io.*;
import java.util.zip.*;

//import android.util.Log;

public class GenerarZIP {
	
	final static int BUFFER = 2048;
	
	public static void makeZIP(String srcFolder){
	// public static void main(String[] args){
		//String srcFolder = "C:/Users/soluciones/Desktop/New folder";
		
	    try {
	        BufferedInputStream origin = null;

	        FileOutputStream    dest = new FileOutputStream(new File(srcFolder+ ".zip"));

	        ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));
	        byte data[] = new byte[BUFFER];

	        File subDir = new File(srcFolder);
	        String subdirList[] = subDir.list();
	        
	        for(String sd:subdirList){
	        	// get a list of files from current directory
                File f = new File(srcFolder+"/"+sd);
                
                if(f.isDirectory()){
                	
	                String files[] = f.list();
	
	                for (int i = 0; i < files.length; i++) {
	                	
	                    System.out.println("MrRobot - Agregando archivo: " + files[i]);
	                    String cadena = files[i].toString();
	                    String extension = "";
	                    int i1 = cadena.lastIndexOf(".");
	                    if (i1 >= 0) {
	                      extension = cadena.substring(i1 + 1);
	                      	
	                    }
	                    if ( extension.equals("pdf") || extension.equals("PDF") ||extension.equals("txt") || extension.equals("TXT") ||extension.equals("csv") || extension.equals("CSV") )
	                    {
			                    System.out.println("MrRobot - Agregando extension: " + extension);
			                    FileInputStream fi = new FileInputStream(srcFolder  + "/"+sd+"/" + files[i1]);
			                    origin = new BufferedInputStream(fi, BUFFER);
			                    ZipEntry entry = new ZipEntry(sd +"/"+files[i1]);
			                    out.putNextEntry(entry);
			                    int count;
			                    
			                    while ((count = origin.read(data, 0, BUFFER)) != -1) {
			                        out.write(data, 0, count);
			                        out.flush();
			                    }
	                    }        
	
	                }
	                
                }else{ //it is just a file
                	 System.out.println("MrRobot - Agregando archivo: " + f);
                	 String cadena = f.toString();
	                   
	                    String extension = "";
	                    int i = cadena.lastIndexOf(".");
	                    if (i >= 0) {
	                      extension = cadena.substring(i + 1);
	                      	
	                    }
	                    if ( extension.equals("pdf") || extension.equals("PDF") ||extension.equals("txt") || extension.equals("TXT") ||extension.equals("csv") || extension.equals("CSV") )
	                    {
	                    	FileInputStream fi = new FileInputStream(f);
	                        origin = new BufferedInputStream(fi, BUFFER);
	                        ZipEntry entry = new ZipEntry(sd);
	                        out.putNextEntry(entry);
	                        int count;
	                        while ((count = origin.read(data, 0, BUFFER)) != -1) {
	                            out.write(data, 0, count);
	                            out.flush();
	                        }
	                    }
	                   
	                  
                    

                }
	                
	        }
	        
	        origin.close();
	        out.flush();
	        out.close();
	        
	        System.out.println("MrRobot -  Zip generado: " + srcFolder + ".zip");
	        
	    } catch (Exception e) {
	    	
	    	 System.out.println("MrRobot - Exception" + e.getMessage());
	    	 e.printStackTrace();
	    	 
	    }
	     
	}   
	
}
