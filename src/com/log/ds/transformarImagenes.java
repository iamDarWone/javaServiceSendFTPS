package com.log.ds;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;

import Decoder.BASE64Decoder;

public class transformarImagenes {
	private static final Logger LOG = Logger.getLogger(transformarImagenes.class);
	
	
	public static boolean convertir(String imageString,String ruta,String nombreImagen) {
		LOG.info("convertir imageString Value is: " +imageString);
		LOG.debug("convertir imageString Value is: " +imageString);
		
		 LOG.info("convertir ruta Value is: " +ruta);
		 LOG.debug("convertir ruta Value is: " +ruta);
		 
		 LOG.info("convertir ruta Value is: " +nombreImagen);
		 LOG.debug("convertir ruta Value is: " +nombreImagen);
		 
		// create a buffered image
		BufferedImage image = null;
		byte[] imageByte;

		BASE64Decoder decoder = new BASE64Decoder();
		try {
			  if("".equals(imageString) ||" ".equals(imageString) || imageString==null){
				  return false; 
			  }
			  else{
					imageByte = decoder.decodeBuffer(imageString);
					ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
					image = ImageIO.read(bis);
					bis.close();
		
					// write the image to a file
					File outputfile = new File(ruta+nombreImagen+".png");
					ImageIO.write(image, "png", outputfile);
					LOG.info("convertir   ok "+ruta+nombreImagen+".png");
					LOG.debug("convertir ok "+ruta+nombreImagen+".png");
					return true;
			 }
			 
		} catch (IOException e) {
			// TODO Auto-generated catch block
		//	e.printStackTrace();
			LOG.info("convertir false e.printStackTrace(); Value is: "+ e.toString());
			LOG.debug("convertir  false e.printStackTrace(); Value is: " +e.getMessage());
			return false;
		}
		
	}
	
	public static boolean insertarImagenPdf(String ruta,String rutaImagen) {
		
		 LOG.info("insertarImagenPdf ruta Value is: " +ruta);
		 LOG.debug("insertarImagenPdf ruta Value is: " +ruta);
		 LOG.info("insertarImagenPdf rutaImagen Value is: " +rutaImagen);
		 LOG.debug("insertarImagenPdf rutaImagen Value is: " +rutaImagen);
		 
		
		try {
			
				  	 Document documento = new Document();
			  		
					 PdfWriter.getInstance(documento, new FileOutputStream(ruta));
					
			  		  documento.open();
			  		 
			  		  Image imagen1 = Image.getInstance(rutaImagen);
			  		  
					  imagen1.setAlignment(Image.ALIGN_CENTER);
					  
		              imagen1.scaleToFit(500f,500f);
		              
		              documento.add(imagen1);
		              
		              documento.close();
		              
					return true;
			 
			 
		} catch (IOException e) {
		
			LOG.info("insertarImagenPdf false e.printStackTrace(); Value is: "+ e.toString());
			LOG.debug("insertarImagenPdf  false e.printStackTrace(); Value is: " +e.getMessage());
			return false;
		}catch (DocumentException e) {
			LOG.info("insertarImagenPdf false e.printStackTrace(); Value is: "+ e.toString());
			LOG.debug("insertarImagenPdf  false e.printStackTrace(); Value is: " +e.getMessage());
			return false;
		}
		
	}

}
